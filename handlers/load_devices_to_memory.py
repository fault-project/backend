# Load all devices from the database to memory
from dataClasses.all_devices import All_Devices
from tinydb import TinyDB, Query
import copy

class Load_Devices_From_DB:
    def __init__(self, ):
        pass
    # Load from the database
    def load_from_database(self) -> dict:
        db = TinyDB('device_data.json')
        device_list = db.all()
        print(f"device_list {device_list}")
        devices = copy.deepcopy({})
        for device in device_list:
            print(device)
            devices[device["device_uuid"]] = device
        print(devices)
        return devices
    
    # Load into the Dataclass
    def load_to_dataclass(devices : dict) -> list:
        All_Devices.device_list = devices
        return All_Devices.device_list





# temp_obj = Load_Devices_From_DB()
# temp_obj.load()