from tinydb import TinyDB, Query


class EmailerDetails:
    def __init__(self):
        self.emailer_addr = None
        
    def fetch_emailer_addr(self):
        if self.emailer_addr:
            print("fetching from class")
            return self.emailer_addr
        db = TinyDB('alarm_service_data.json')
        Service_Query = Query()
        res1 = db.search(Service_Query.name == "emailer")
        if not res1:
            return str(-1)
        self.emailer_addr = res1[0]["uri"]
        return res1[0]["uri"]
        
    def set_emailer_addr(self, uri : str):
        db = TinyDB('alarm_service_data.json')
        Service_Query = Query()
        if self.fetch_emailer_addr() != '-1':
            db.update({'uri': uri}, Service_Query.name == "emailer")
            return
        db.insert({'uri' : uri, 'name' : "emailer"})
        self.emailer_addr = uri



t = EmailerDetails()

t.set_emailer_addr("abcdefghqweqwewq")

print(t.fetch_emailer_addr())