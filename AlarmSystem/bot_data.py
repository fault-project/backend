from tinydb import TinyDB, Query


class BotDetails:
    def __init__(self):
        self.bot_addr = None
    def fetch_bot_addr(self):
        if self.bot_addr:
            print("fetching from class")
            return self.bot_addr
        db = TinyDB('alarm_service_data.json')
        Service_Query = Query()
        res1 = db.search(Service_Query.name == "discord-bot")
        if not res1:
            return str(-1)
        self.bot_addr = res1[0]["uri"]
        return res1[0]["uri"]
        
    def set_bot_addr(self, uri : str):
        db = TinyDB('alarm_service_data.json')
        Service_Query = Query()
        if self.fetch_bot_addr() != '-1':
            db.update({'uri': uri}, Service_Query.name == "discord-bot")
            return
        db.insert({'uri' : uri, 'name' : "discord-bot"})
        self.bot_addr = uri



# t = BotDetails()

# t.set_bot_addr("abcdefgh")

# print(t.fetch_bot_addr())