# from ./emailer_data import EmailerDetails
# from api import fetch_emailer_addr
from tinydb import TinyDB, Query
import requests
import json
import yagmail
import pymongo
from pymongo import MongoClient
import os

addr = os.path.join("C:/Users/Gagandeep Bhatia/Desktop/project-sem-8/code/backend/AlarmSystem/device_alarm.json")

class DeviceAlarm:
    def __init__(self):
        pass

    def set_device_alarm(self, device_uuid, bot_alarm_status = True, email_alarm_status = True):
        db = TinyDB(addr)
        DeviceQuery = Query()
        print(f"alarm staus reieved {bot_alarm_status} {email_alarm_status}")
        if db.contains(DeviceQuery.uuid == device_uuid):
            db.update({"uuid" : device_uuid, "bot-alarm-status" : bot_alarm_status, "email_alarm_status": email_alarm_status})
            return
        db.insert({"uuid" : device_uuid, "bot-alarm-status" : bot_alarm_status, "email_alarm_status": email_alarm_status})
        print("device alarm set to true")
        return

    def fetch_device_alarm(self, device_uuid):
        db = TinyDB(addr)
        DeviceQuery = Query()
        res1 = db.search(DeviceQuery.uuid == device_uuid)
        if not res1:
            return "-1"
        return [res1[0]["bot-alarm-status"], res1[0]["email_alarm_status"]]
    
    def make_discord_alarm_call(self, device_uuid, message):
        resp = requests.post("https://faulty-friday.herokuapp.com/add_machine", json={"machine_id" : device_uuid, "user_id" : message})
        print(resp.text)
        print(resp.status_code)
    
    def make_email_alarm_call(self, device_uuid, message):
        to = self.fetch_emails_from_mongo()
        if len(to) == 0:
            return json.dumps("No emails found")
        user = "majorearlyfaultdetect@gmail.com"
        app_password = "cyxsbyldljncidza"
        content = [message, "For the Device id " + device_uuid]
        for t in to:
            with yagmail.SMTP(user,app_password) as yag:
                yag.send(t,"Error Detected",content)

        return json.dumps("Success")


    def fetch_emails_from_mongo(self):
        # code to fetch emails from mongo here
        email = ["bhatiagagan24@gmail.com", "vatsaltulshyan@gmail.com"]
        return email
        cluster = MongoClient("mongodb+srv://team86:team86@early-fault-detection.cnjve.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
        db = cluster["early_fault_detection"]
        emails = db.data
        email_list = []
        for f in emails.find():
            email_list.append(f["email"])
        return email_list

# obj1 = DeviceAlarm()
# obj1.set_device_alarm("random-uuid", bot_alarm_status=True, email_alarm_status=True)
# # # print(obj1.fetch_device_alarm(device_uuid="random-uuid"))
# obj1.make_discord_alarm_call("random-uuid", "random-message")


obj1 = DeviceAlarm()
obj1.fetch_emails_from_mongo()
