
import requests
from time import sleep
import json

class Requester:
    def __init__(self, uri : str, machine_id):
        # self.uri = "http://127.0.0.1:5000/store-log"
        self.uri = uri
        # self.port = port
        self.body = machine_id + "__=__"
    def send_request(self, log_value):
        statement = self.uri + "?log=" + self.body + log_value
        resp = requests.get(statement)
        print(resp)



class EventHandler:
    def __init__(self, file_name : str, requester_obj : Requester):
        self.file_name = file_name
        self.last_line_num = 0
        self.requester_obj = requester_obj
    def check_file(self):
        while True:
            sleep(2)
            file = open(self.file_name)
            file_content = file.readlines()
            file_length = len(file_content)
            if file_length > self.last_line_num:
                for line_number in range(self.last_line_num+1, file_length):
                    self.requester_obj.send_request(log_value = file_content[line_number])
            self.last_line_num = file_length

if __name__ == '__main__':
    # Taking the file name as the user input
    file_name = input('Enter the File Name :: => ')
    # Reading the message que url and the port from the file 
    file = open('deviceId.json')
    data = json.load(file)
    uri = data["message_queue_address"]
    machine_id = data["device_uuid"]
    requester_obj = Requester(machine_id=machine_id, uri=uri)
    event_handler_obj = EventHandler(file_name=file_name, requester_obj=requester_obj)
    event_handler_obj.check_file()





        
