from dataclasses import dataclass, asdict
from tinydb import TinyDB, Query
import json
from dataClasses.device_info import Device_Info


class Device_Info_Database:
    
    def __init__(self):
        pass
    
    def fetch_device_info_by_id(self, device_uuid):
        db = TinyDB('device_data.json')
        print(device_uuid)
        Device_Query = Query()
        res1 = db.search(Device_Query.id == str(device_uuid))
        print("res1 " , res1 )
        if len(res1) == 0:
            return json.dumps({"msg" : "Device Not Found"})
        resp = json.dumps(res1[0])
        print(resp)
        print(type(resp))
        return resp

    def temporary_device(self):
        db = TinyDB('device_data.json')
        # db.truncate()
        db.insert({'id' : 'temporary1', 'port' : 2000, 'other' : 'random information here'})
    
    def register_a_device(self, deviceDataClass : Device_Info):
        db = TinyDB('device_data.json')
        f = asdict(deviceDataClass)
        db.insert(f)
        print(f)

        



# obj1 = Device_Info()
# obj1.temporary_device()
# obj1.fetch_device_info_by_id('temporary1')