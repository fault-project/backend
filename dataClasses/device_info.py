# Data class for device info

from dataclasses import dataclass, asdict

@dataclass(init=False)
class Device_Info:
    device_name : str
    device_uuid : str
    device_addr : str
    creator_id : str
    other_details : str

    def parse_data(self, **kwargs):
        self.device_name = kwargs['device_name']
        self.creator_id = kwargs['creator_id']
        self.device_addr = kwargs['device_addr']
        self.device_uuid = kwargs['device_uuid']
        self.other_details = kwargs['other_details']

    def return_device_name_temp_function(self):
        print(self.device_name)




# obj1 = Device_Info()
# obj1.parse_data(**{
#     'device_name' : 'temp_device_name',
#     'creator_id' : 'temp_creator',
#     'device_addr' : 'temp_device_addr',
#     'device_uuid' : 'temp_device_uuid',
#     'other_details' : 'temp_other_details'
# })
# obj1.return_device_name_temp_function()
