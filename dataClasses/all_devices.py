from dataclasses import dataclass


@dataclass(init=False)
class All_Devices:
    device_list : dict = None


# obj1 = device_info.Device_Info()
# obj1.parse_data(**{
#     "device_name" : "temp_name",
#     "device_addr" : "temp_address",
#     "creator_id" : "temp_id_1",
#     "other_details" : "temp_details",
#     "auth_key" : "random_auth_key_not_in_db_here",
#     "device_uuid" : "100000"
# })
# obj2 = device_info.Device_Info()
# obj2.parse_data(**{
#     "device_name" : "temp_name",
#     "device_addr" : "temp_address",
#     "creator_id" : "temp_id_1",
#     "other_details" : "temp_details",
#     "auth_key" : "random_auth_key_not_in_db_here",
#     "device_uuid" : "100000"
# })

# All_Devices.device_list = [obj1]


# print(All_Devices.device_list)


# All_Devices.device_list.append(obj2)

# print("\n \n " , All_Devices.device_list)