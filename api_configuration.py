# For Configuring the API using a CLI


class Api_Config:
    api_port_number = 5000
    log_queue_port = 0
    script_path = None
    email_database_uri = None
    email_database_uri_column_name = None

    def __init__(self):
        print("Welcome to the Setup")
    def set_port_number(self):
        api_port_option = input("Press y/Y for Default port 5000 // Press any other for custom port")
        if api_port_option != "y" and api_port_option != "Y":
            port_num = int(input("Remember this port. Frontend will need this for its function :: "))
            self.api_port_number = port_num
        print("\n")
        queue_port = int(input("Enter the socket port from where to consume logs from message queue"))
        self.log_queue_port = queue_port
        print("\n")
        automated_script_option = input("Do you want to set up Automated Scripts? Press y/Y for yes and any other key for no")
        if automated_script_option == "y" and automated_script_option == "Y":
            self.script_path = input()
        alert_system_option = input("Do you want to set up the email alert system? Press y/Y for yes and anything else for no")
        if alert_system_option == "y" and alert_system_option == "Y":
            self.email_database_uri = input("database uri with emails :: ")
            self.email_database_uri_column_name = input("column name :: ")
            