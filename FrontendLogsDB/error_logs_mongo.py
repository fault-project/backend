import pymongo
from pymongo import MongoClient
import json

class MongoOps:
    def __init__(self):
        self.uri = "mongodb+srv://team86:team86@early-fault-detection.cnjve.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
    
    def fetch_errors(self, machine_id):
        cluster = MongoClient(self.uri)
        db = cluster["logs"]
        log_doc = db["error_logs"]
        answer = []
        print(f"machine id => " , machine_id)
        for f in log_doc.find():
            print(f"{f}")
            if f["m-id"] == machine_id:
                answer.append([f["m-id"], f["log-val"], f["time"]])
        answer.sort(key= lambda x : x[2])
        return json.dumps(answer)
        
    def fetch_logs(self, machine_id):
        cluster = MongoClient(self.uri)
        db = cluster["logs"]
        print(f"machine id => " , machine_id)
        machine_for_log = db[machine_id]
        fetched_logs = []
        for f in machine_for_log.find():
            fetched_logs.append([f["log-val"], f["time"]])
        fetched_logs.sort(key = lambda x:x[0])
        return json.dumps(fetched_logs)
    
    def fetch_number_of_logs(self):
        cluster = MongoClient(self.uri)
        db = cluster["logs"]
        log_doc = db["error_logs"]
        counter = 0
        for f in log_doc.find():
            counter = counter + 1
        print(counter)
        return counter


# obj1 = MongoOps()
# obj1.fetch_number_of_logs()