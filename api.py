from logging import Handler
from flask import Flask, request, send_file
from dataClasses.device_info import Device_Info
from devices.device_database import Device_Info_Database
# import devices.device_info as device_information
from handlers.load_message_queue_data import Message_Queue_Data
from AlarmSystem import bot_data, emailer_data, deviceAlarm
import json, uuid
from FrontendLogsDB import error_logs_mongo
from handlers import load_devices_to_memory
from flask_cors import CORS, cross_origin
from configparser import ConfigParser
config = ConfigParser()
api = Flask(__name__)
config.read("./project.conf")
ec2_addr = config["Utils"]["ec2_addr"]
print(ec2_addr)
CORS(api)

auth_key = "temp"

@api.route("/device/download/setup-script/<type>", methods=['GET'])
def send_device_registration_script(type):
    print(type)
    try:
        if type == "raw":
            return send_file('./deviceSoftwares/main.py', download_name='main.py')
        else:
            return send_file('./deviceSoftwares/main.exe', attachment_filename='main.exe')
        
    except Exception as e:
        print(e)
        return str(e)

@api.route('/device/download/event-handler', methods=['GET'])
def return_event_handler():
    return send_file('./deviceSoftwares/handler.py', download_name='handler.py')


@api.route("/device/register_a_device", methods=['POST'])
def device_register():
    req = request.json

    if ("auth_key" not in req.keys()):
        return json.dumps({"msg": "send auth_key"})
    auth_key_sent = req['auth_key']
    if auth_key_sent != auth_key:
        return json.dumps({"msg" : "Not Verified", "code" : -1})

    generated_device_id = str(uuid.uuid1())
    req['device_uuid'] = generated_device_id
    obj1 = Device_Info()
    obj1.parse_data(**req)
    obj2 = Device_Info_Database()
    obj2.register_a_device(obj1)
    del obj1, obj2
    obj3 = Message_Queue_Data()
    message_queue_dict = obj3.fetch_message_queue_information()
    message_queue_addr = "http://"+ec2_addr+":5001/store-log"
    return json.dumps({
            "device_uuid" : generated_device_id, 
            "code" : 1, "msg" : "success", 
            "message_queue_address" : message_queue_addr,
            # "message_queue_port" : message_queue_dict['port']
        })





@api.route('/web/fetch/devices/info/all', methods=['GET'])
def fetch_all_devices():
    devices_obj1 = load_devices_to_memory.Load_Devices_From_DB()
    devices = devices_obj1.load_from_database()
    return devices



@api.route("/web/fetch/device/info", methods=['GET'])
def fetch_device_info():
    deviceId = request.args.get("deviceId")
    print(deviceId)
    # obj1 = Device_Info_Database()
    # device_data = obj1.fetch_device_info_by_id(deviceId)
    devices_obj1 = load_devices_to_memory.Load_Devices_From_DB()
    devices = devices_obj1.load_from_database()
    # print(type(devices))
    if deviceId not in devices:
        return json.dumps("Device not found")
    return json.dumps(devices[deviceId])
    # del obj1
    # return device_data


@api.route("/web/set/bot-uri", methods=['POST'])
def set_bot_uri():
    req = request.get_json()
    bot_addr = req['bot-addr']
    bot_details_obj = bot_data.BotDetails()
    bot_details_obj.set_bot_addr(bot_addr)
    return "Success"

@api.route("/web/fetch/bot-uri", methods=['GET'])
def fetch_bot_uri():
    bot_details_obj = bot_data.BotDetails()
    return json.dumps(bot_details_obj.fetch_bot_addr())


@api.route("/web/set/emailer-uri", methods=['POST'])
def set_emailer_uri():
    req = request.get_json()
    emailer_addr = req['email-addr']
    emailer_details_obj = emailer_data.EmailerDetails()
    emailer_details_obj.set_emailer_addr(emailer_addr)
    return "Success"

@api.route("/web/fetch/emailer-db-uri", methods=['GET'])
def fetch_emailer_addr():
    emailer_data_obj = emailer_data.EmailerDetails()
    return json.dumps(emailer_data_obj.fetch_emailer_addr())


@api.route("/web/set/device/alarm", methods=['POST'])
def set_device_services():
    req = json.loads(request.data)
    device_uuid = req["device-id"]
    print(req)
    email_status = req["email-stat"]
    bot_status = req["bot-stat"] 
    print(f"{email_status} {bot_status}")
    device_alarm_obj = deviceAlarm.DeviceAlarm()
    device_alarm_obj.set_device_alarm(device_uuid=device_uuid, bot_alarm_status=bot_status, email_alarm_status=email_status)
    return json.dumps("Success")

@api.route("/web/fetch/device/alarm", methods=['GET'])
def fetch_device_services():
    device_uuid = request.args.get("uuid")
    device_alarm_obj = deviceAlarm.DeviceAlarm()
    resp = device_alarm_obj.fetch_device_alarm(device_uuid=device_uuid)
    del device_alarm_obj
    return json.dumps(resp)


@api.route("/web/alarm/bot/make-call", methods=['GET'])
def make_discord_bot_call():
    device_uuid = request.args.get("device_uuid")
    message = request.args.get("message_value")
    device_alarm_obj = deviceAlarm.DeviceAlarm()
    device_alarm_obj.make_discord_alarm_call(device_uuid=device_uuid, message=message)
    return json.dumps(device_alarm_obj.fetch_device_alarm(device_uuid=device_uuid))

@api.route("/web/alarm/email/make-call", methods=['GET'])
def send_emails():
    device_uuid = request.args.get("device_uuid")
    message = request.args.get("message_value")
    device_alarm_obj = deviceAlarm.DeviceAlarm()
    device_alarm_obj.make_email_alarm_call(device_uuid=device_uuid, message=message)
    return json.dumps(device_alarm_obj.fetch_device_alarm(device_uuid=device_uuid))


@api.route("/web/fetch/error-logs", methods=['GET'])
def fetch_error_logs():
    device_uuid = request.args.get("device_uuid")
    print(f"device_uuid {device_uuid}")
    mongo_ops_obj = error_logs_mongo.MongoOps()
    return mongo_ops_obj.fetch_errors(device_uuid)

@api.route("/web/fetch/machine-logs", methods=['GET'])
def fetch_machine_logs():
    device_uuid = request.args.get("device_uuid")
    mongo_ops_obj = error_logs_mongo.MongoOps()
    return mongo_ops_obj.fetch_logs(device_uuid)

@api.route("/web/fetch/no-of-errors", methods=['GET'])
def fetch_no_errors():
    mongo_ops_obj = error_logs_mongo.MongoOps()
    return json.dumps(mongo_ops_obj.fetch_number_of_logs())

@api.route("/setup/fetch/uri/cache-service", methods=['GET'])
def return_cache_address():
    addr = ""
    return addr


@api.route("/temp")
def temp():
    return "hello"


if __name__ == '__main__':
    api.run(debug=True, host = "0.0.0.0",port=5000)